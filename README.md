

## Files
* [m.pfm](http://www.pauldebevec.com/Research/HDR/PFM/) - pfm file (memorial.pfm) by Paul Debevec
* [main.cpp](main.cpp) - cpp progra. One file, slightly modified version of [the original files by Antoine TOISOUL LE CANN](https://github.com/antoinetlc/PFM_ReadWrite)


### m.pfm
First lines of binary m.pfm read using [Bless Hex Editor](https://github.com/afrantzis/bless) 
```
PF
512 768
-1.000000
```

or xxd:
```
xxd m.pfm|head
00000000: 5046 0a35 3132 2037 3638 0a2d 312e 3030  PF.512 768.-1.00
00000010: 3030 3030 0a00 8020 3c00 001e 3b00 0038  0000... <...;..8
```

Description by Paul Debevec
>>>
the format begins with three lines of text specifying the image size and type, and then continues with raw binary image data for the rest of the file.  
The text header of a .pfm file takes the following form:  

[type]  
[xres] [yres]  
[byte_order]  

Each of the three lines of text ends with a 1-byte Unix-style carriage return: 0x0a in hex, not the Windows/DOS CR/LF combination.   
The "[type]" is one of "PF" for a 3-channel RGB color image, or "Pf" for a monochrome single-channel image.  
"[xres] [yres]" indicates the x and y resolutions of the image.   
"[byte_order]" is a number used to indicate the byte order within the file. A positive number (e.g. "1.0") indicates big-endian, with the most significant byte of each 4-byte float first. If the number is negative (e.g. "-1.0") this indicates little-endian, with the least significant byte first.   
There are no comments in these files.
>>>

So it is a rgb color image  512x768 pixels with little-endian byte order 


## PFM_ReadWrite
PFM file reader and writer.

This open source program provides functions to read and write PFM files. PFM files can be loaded to an OpenCV matrix (Mat) and saved from an OpenCV Mat to a PFM file. A description of the PFM file format can be found here : http://www.pauldebevec.com/Research/HDR/PFM/.

Note that in the current version the files are loaded and saved using little endian format only.

#### VERSION
Version 1.0

#### Compilation
This program has been compiled and tested with:
* OpenCV 2.4.11 on Windows ans Linux environments ( [original repo](https://github.com/antoinetlc/PFM_ReadWrite))
* OpenCV 3 ( this cloned repo)



```bash
g++ main.cpp `pkg-config --cflags --libs opencv`

```

run : 

```bash
./a.out
```


#### LICENSE

PFM_ReadWrite. Author :  Antoine TOISOUL LE CANN. Copyright © 2016 Antoine TOISOUL LE CANN, Imperial College London. All rights reserved.


PFM_ReadWrite is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. PFM_ReadWrite is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details. You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.


## Raw data
RAW files are not supported in OpenCV



To get full-colored image from [raw  data](https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D/gfile#raw) one need to
* do [demosaic](https://en.wikipedia.org/wiki/Demosaicing) 
  * apply white balance
  * interpolate color data for missing color values
  * convert to output color space
  * do gamma correction
  
### Libraries
* [libraw](https://www.libraw.org)
* [xphotp](https://github.com/opencv/opencv_contrib/tree/master/modules/xphoto)
* [NumPy](https://stackoverflow.com/questions/18682830/opencv-python-display-raw-image)
* [libpnmio](https://github.com/nkkav/libpnmio) c library fo r P*M files ( also pfm) by Nikolaos Kavvadias 

## OpenCV
* [doc for 4. version](https://www.ccoderun.ca/programming/doxygen/opencv/group__imgcodecs.html)
* [image processing](https://docs.opencv.org/3.4/d7/dbd/group__imgproc.html)
* [hdr in OpenCV](https://docs.opencv.org/3.0-beta/doc/tutorials/photo/hdr_imaging/hdr_imaging.html#hdrimaging)


## git

```git
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/pfm_cpp_opencv.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

local repo: ~/cpp/opencv/pfm/PFM_ReadWrite$ 

